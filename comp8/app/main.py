"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
import os
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, StandardScaler
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import Dense
import pandas as pd
import numpy as np

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "#Anushree Sarkar#"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="comp8",
                   level=logging.INFO)


class Comp8(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, xpresso_run_name, parameters_filename,
                 parameters_commit_id):
        super().__init__(name="Comp8",
                         run_name=xpresso_run_name,
                         params_filename=parameters_filename,
                         params_commit_id=parameters_commit_id)
        # if you have specified parameters_filename or parameters_commit_id,
        # the run parameters can be accessed from self.run_parameters
        self.dataset = pd.read_csv("/data/churndata/Churn_Modelling.csv")
        self.model = Sequential()
        self.x_test = None
        self.y_test = None
        """ Initialize all the required constants and data here """

    def start(self, xpresso_run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            xpresso_run_name: xpresso run name which is used by base class to
            identify the current run. It must be passed. While running as
            pipeline, xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=xpresso_run_name)
            X = self.dataset.iloc[:, 3:13]
            Y = self.dataset.iloc[:, 13]

            # pre-processing nominal data
            le_X1 = LabelEncoder()
            le_X2 = LabelEncoder()
            X.iloc[:, 1] = le_X1.fit_transform(X.iloc[:, 1])
            X.iloc[:, 2] = le_X2.fit_transform(X.iloc[:, 2])

            ct = ColumnTransformer(
                [('one_hot_encoder', OneHotEncoder(), [1])],
                remainder='passthrough')

            X = np.array(ct.fit_transform(X), dtype=np.float)
            X = X[:, 1:]

            # Splitting Data
            X_train, self.x_test, Y_train, self.y_test = train_test_split(X, Y, test_size=0.2, random_state=0)

            # Create dataframe for test data to be saved in pachyderm
            self.x_test = pd.DataFrame(self.x_test)
            self.y_test = pd.DataFrame(self.y_test)
            # Feature Scaling
            sc = StandardScaler()
            X_train = sc.fit_transform(X_train)

            # Model creating
            self.model.add(Dense(6, kernel_initializer='uniform', activation='relu', input_dim=11))
            self.model.add(Dense(6, kernel_initializer='uniform', activation='relu'))
            self.model.add(Dense(1, kernel_initializer='uniform', activation='sigmoid'))
            # Compile and fit the model
            self.model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
            self.model.fit(X_train, Y_train, batch_size=10, epochs=100)
            # evaluate the keras model
            loss, accuracy = self.model.evaluate(X_train, Y_train)
            print(f'Accuracy: {accuracy * 100},\n Loss: {loss}')
            self.send_metrics(desc="Training Accuracy", val=accuracy * 100,key="accuracy")
            self.send_metrics(desc="Training Loss",val=loss,key="loss")
            logging.info(accuracy * 100)

            ### $xpr_param_pipeline_job_import

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    def send_metrics(self,desc,val,key):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": desc},
                "metric": {key: val}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=True, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        logging.info("Saving model")
        logging.info("Saving test_data")
        path2 = "/output/test_data"
        if not os.path.exists(self.OUTPUT_DIR):
            os.makedirs(self.OUTPUT_DIR)
        self.model.save(os.path.join(self.OUTPUT_DIR, 'churn_model.h5'))
        if not os.path.exists(path2):
            os.makedirs(path2)
        self.x_test.to_csv(os.path.join(path2, 'xtest.csv'))
        self.y_test.to_csv(os.path.join(path2, 'ytest.csv'))

        logging.info("Saved model...")
        logging.info("Saved csvs")
        try:
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    run_name = ""
    params_filename = None
    params_commit_id = None
    if len(sys.argv) >= 2:
        run_name = sys.argv[1]
    if len(sys.argv) >= 4:
        params_filename = sys.argv[2] if sys.argv[2] != "None" else None
        params_commit_id = sys.argv[3] if sys.argv[3] != "None" else None

    pipeline_job = Comp8(run_name, params_filename, params_commit_id)
    pipeline_job.start(xpresso_run_name=run_name)
