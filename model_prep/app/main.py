"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
import os
import pickle
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score,confusion_matrix
from sklearn.model_selection import train_test_split,GridSearchCV
from xgboost import XGBClassifier

# Following two imports are required for Xpresso. Do not remove this.
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "### Author ###"

# To use the logger please provide the name and log level
#   - name is passed as the project name while generating the logs
#   - level can be DEBUG, INFO, WARNING, ERROR, CRITICAL
logger = XprLogger(name="model_prep",
                   level=logging.INFO)


class ModelPrep(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self, xpresso_run_name, parameters_filename,
                 parameters_commit_id):
        super().__init__(name="ModelPrep",
                         run_name=xpresso_run_name,
                         params_filename=parameters_filename,
                         params_commit_id=parameters_commit_id)
        # if you have specified parameters_filename or parameters_commit_id,
        # the run parameters can be accessed from self.run_parameters
        """ Initialize all the required constants and data here """
        self.df = pd.read_csv(self.run_parameters["filepath"])
        print(self.df.head(), flush=True)
        logging.info(self.df.info())
        self.model = XGBClassifier(n_estimators=100)
        self.x_test = None
        self.y_test = None

    def start(self, xpresso_run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            xpresso_run_name: xpresso run name which is used by base class to
            identify the current run. It must be passed. While running as
            pipeline, xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=xpresso_run_name)
            logging.info(self.df.drop(["RowNumber", 'CustomerId'], axis=1).describe().T)

            """Correlation Heatmap of the columns"""
            # plt.style.use("ggplot")
            # f, ax = plt.subplots(figsize=(10, 8))
            # sns.heatmap(self.df.drop(["RowNumber", 'CustomerId'], axis=1).corr(), robust=True, fmt='.1g', linewidths=1.3,
            #             linecolor='gold', annot=True, )

            """Countries with the less and the most common use of credit cards"""
            # plt.figure(figsize=(12, 6))
            # sns.countplot(x="HasCrCard", hue="Geography", data=self.df, palette="husl")
            logging.info("Grouped Country wise Crad Holders Count")
            logging.info(self.df.groupby('Geography')["HasCrCard"].sum())
            # plt.figure(figsize=(12, 6))
            # sns.countplot(x="HasCrCard", hue="Exited", data=self.df, palette="husl")
            logging.info("Grouped Country wise Exit count")
            logging.info(self.df.groupby('Geography')["Exited"].sum())

            """Countries with highest salary"""
            avg_salaries = self.df.groupby("Geography").mean()["EstimatedSalary"]
            logging.info("Average Salaries according to Countries:")
            logging.info(avg_salaries)
            """As we can see the customers without credit card exited 39-52 most(median=45)
                Credit card owners also same behavior, exited ages 38-51 most(median=45)
                As a result for age variable credit card usage does not effect the exit decision directly. 
                This can may be the occur fot that reason in most cases banks give credit card to customers automatically."""
            # fig = px.box(self.df, x="HasCrCard", y="Age", color='Exited')
            # fig.update_layout(title_text="Credit Card Usage & Age - With Outliers(Exited-Not Exited groups)")
            # fig.show()

            """Rates of credit card’s usage according to gender"""
            # fig = px.parallel_categories(self.df, dimensions=['Gender', 'Geography', 'Exited'],
            #                              color="Exited", color_continuous_scale=px.colors.sequential.Inferno,
            #                              labels={'Gender': 'Gender(Female,Male)', 'Exited': 'Exited(0:No,1:Yes)'})
            # fig.update_layout(title_text="Gender-Geography-Exited-Not Exited Schema")
            # fig.show()

            """Countries with highest credit score"""
            logging.info("Countries with Highest credit score")
            logging.info(self.df.groupby("Geography")["CreditScore"].mean())
            # fig = px.box(self.df, x="Geography", y="CreditScore", color='Exited')
            # fig.update_layout(
            #     title_text="The country with the highest credit score(mean)-With Outliers(Exited-Not Exited groups)")
            # fig.show()

            #     Feature Engg
            #     Age Grouping
            logging.info("Feature Engg")
            logging.info("Age grouping")
            age_group_data = [None] * len(self.df['Age'])
            for i in range(len(self.df['Age'])):
                if self.df['Age'][i] < 30:
                    age_group_data[i] = 'Young'
                elif self.df['Age'][i] >= 30 and self.df['Age'][i] < 40:
                    age_group_data[i] = 'Young-Adults'
                elif self.df['Age'][i] >= 40 and self.df['Age'][i] < 50:
                    age_group_data[i] = 'Adults'
                elif self.df['Age'][i] >= 50 and self.df['Age'][i] < 60:
                    age_group_data[i] = 'Elderly-Adults'
                elif self.df['Age'][i] >= 60 and self.df['Age'][i] < 74:
                    age_group_data[i] = 'Old'
                else:
                    age_group_data[i] = 'Very-Old'

            self.df['age_group'] = age_group_data

            age74 = self.df[(self.df["Age"] >= 74)]
            age74["Exited"].value_counts()
            # We dropped the 2 lines that is outlier of the above 73 ages.
            self.df.drop([3110, 3531], axis=0, inplace=True)
            g = sns.factorplot(x="age_group", y="Exited", data=self.df, kind="bar")
            plt.xticks(rotation=45)
            g.set_ylabels("Exited")
            plt.savefig("/data/age_grp_vs_exited.png")
            logging.info("Image saved age_grp_vs_exited.png")

            # Grouping according to credit score greater that 405
            logging.info("Grouping according to credit score greater that 405")
            self.df['new_credit'] = self.df.apply(lambda row: 0, axis=1)
            self.df.loc[self.df['CreditScore'] <= 405, 'new_credit'] = 0
            self.df.loc[self.df['CreditScore'] > 405, 'new_credit'] = 1
            logging.info(self.df["new_credit"].value_counts())
            # sns.factorplot(x="new_credit", y="Exited", data=self.df, kind="bar")
            # plt.xticks(rotation=45)
            # plt.ylabel("Exited(Precent)")

            # Data Preparation
            # Creating dummy variables
            logging.info("Data Preparation")
            logging.info("Creating dummy variables")
            gender_dummies = self.df.replace(to_replace={'Gender': {'Female': 0, 'Male': 1}})
            a = pd.get_dummies(self.df['Geography'], prefix="Geo_dummy")
            c = pd.get_dummies(self.df['age_group'], prefix="Age_dummy")
            frames = [gender_dummies, a, c]
            self.df = pd.concat(frames, axis=1)
            self.df = self.df.drop(
                ["RowNumber", "Geography", "Surname", "CustomerId", 'Age', 'age_group', 'Geography', "CreditScore"],
                axis=1)
            logging.info("Transformed Dataset:")
            logging.info(self.df.head())

            X = self.df.drop(["Exited"], axis=1)  # Independent value
            Y = self.df["Exited"]  # Depended value
            # fit scaler on training data
            norm = MinMaxScaler().fit(X)
            # transform independent data
            x_norm = norm.transform(X)
            x_train, self.x_test, y_train, self.y_test = train_test_split(x_norm, Y, test_size=0.3, random_state=42)

            # Train the Model
            logging.info("Training Model")
            self.model = self.model.fit(x_train, y_train)
            # Model Score without tuning
            y_pred = self.model.predict(self.x_test)
            xgb_score = accuracy_score(self.y_test, y_pred) * 100
            logging.info(f"Score prior to tuning: {xgb_score}")
            self.send_metrics("Score Before Tuning", val=xgb_score,key="xgb_score")
            # Tuning the Model
            xgb_params = {
                'n_estimators': [50, 100, 200],
                'subsample': [0.6, 0.8, 1.0],
                'max_depth': [1, 2, 3, 4],
                'learning_rate': [0.1, 0.2, 0.3, 0.4, 0.5],
                "min_samples_split": [1, 2, 4, 6]}
            self.model = XGBClassifier()
            xgb = GridSearchCV(self.model, xgb_params, verbose=0, n_jobs=-1, cv=3)
            xgb = xgb.fit(x_train, y_train)
            logging.info("Best parameters after model Tuning")
            logging.info(xgb.best_params_)
            eval_set = [(x_train, y_train), (self.x_test, self.y_test)]
            #   Setting the best parameters into the model
            logging.info("Training model again with the best parameters")
            self.model = XGBClassifier(learning_rate=0.3,
                                       max_depth=2,
                                       min_samples_split=1,
                                       n_estimators=100,
                                       subsample=1.0, random_state=42). \
                fit(x_train, y_train,
                    eval_metric=["error", "logloss"], eval_set=eval_set)

            #   Testing on test data
            logging.info("Model Testing")
            y_pred = self.model.predict(self.x_test)
            xgbm_score = (accuracy_score(self.y_test, y_pred) * 100)
            self.send_metrics(desc="Score after Tuning the model", key="xgbm_score",val=xgbm_score)
            logging.info(f"Score after Model Tuning: {xgbm_score}")
            xgbm_cm = confusion_matrix(self.y_test, y_pred)
            self.send_metrics(desc="Confusion Matrix", key="xgbm_cm",val=xgbm_cm)
            logging.info(f"Confusion Matrix: \n {xgbm_cm}")
            # Storing the results
            results = self.model.evals_result_
            logloss = results['validation_0']['logloss']
            error = results['validation_0']['error']
            self.send_metrics(desc="Logloss",key="logloss", val=logloss)
            self.send_metrics(desc="Classification error", key="error",val=error)
            epochs = len(results['validation_0']['error'])
            x_axis = range(0, epochs)

            #   Plot the LogLoss
            fig, ax = plt.subplots(figsize=(12, 12))
            ax.plot(x_axis, results['validation_0']['logloss'], label='Train')
            ax.plot(x_axis, results['validation_1']['logloss'], label='Test')
            ax.legend()
            plt.ylabel('Log Loss')
            plt.title('XGBoost Log Loss')
            plt.savefig("/data/xgb_logloss.png")
            logging.info("Image saved xgb_logloss.png")

            #  Plot the Classification error

            fig, ax = plt.subplots(figsize=(12, 12))
            ax.plot(x_axis, results['validation_0']['error'], label='Train')
            ax.plot(x_axis, results['validation_1']['error'], label='Test')
            ax.legend()
            plt.ylabel('Classification Error')
            plt.title('XGBoost Classification Error')
            plt.savefig("/data/xbg_clf_error.png")
            logging.info("Image saved xgb_clf_error.png")

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed(success=True)

    def send_metrics(self, desc, key,val):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": desc},
                "metric": {key: val}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=True, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        logging.info("Saving model")
        logging.info("Saving test_data")
        if not os.path.exists(self.OUTPUT_DIR):
            os.makedirs(self.OUTPUT_DIR)
        pickle.dump(self.model, open((os.path.join(self.OUTPUT_DIR, 'tuned_churn_model.pkl')), "wb"))
        try:
            super().completed(push_exp=push_exp, success=success)
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()
            sys.exit(1)


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    run_name = ""
    params_filename = None
    params_commit_id = None
    if len(sys.argv) >= 2:
        run_name = sys.argv[1]
    if len(sys.argv) >= 4:
        params_filename = sys.argv[2] if sys.argv[2] != "None" else None
        params_commit_id = sys.argv[3] if sys.argv[3] != "None" else None

    pipeline_job = ModelPrep(run_name, params_filename, params_commit_id)
    pipeline_job.start(xpresso_run_name=run_name)
